export default class AmoContactForm {
    constructor(contactFormSelector) {
        this.elemForm = document.querySelector(contactFormSelector);
        this.successElem = document.querySelector('.success-status');
        this.failedElem = document.querySelector('.danger-status');
    }

    init() {
        this.__setFormHandler();
    }

    __setFormHandler() {
        this.elemForm.addEventListener('submit', (event) => {
            event.preventDefault();
            const DataForm = new FormData(this.elemForm);
            this.__sendAsyncContactData(this.__processingInputFormData(DataForm))
                .then((body) => {
                    console.log(body);
                    this.elemForm.reset();
                    this.__showSendStatus(this.successElem);
                })
                .catch((err) => {
                    console.log(err);
                    this.__showSendStatus(this.failedElem);
                });
        });
    }

    __showSendStatus(statusElem) {
        statusElem.classList.toggle('hidden');
        setTimeout(() => {
            statusElem.classList.toggle('hidden');
        }, 5000);
    }

    __processingInputFormData(dataForm) {
        const contactObject = [...dataForm.entries()].reduce((accum, [inputName, value]) => {
            accum[inputName] = value;
            return accum;
        }, {});
        return contactObject;
    }

    async __sendAsyncContactData(contactDataObject) {
        try {
            const requestResponse = await fetch('/php/index.php', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(contactDataObject)
            });
            if (Math.floor(requestResponse.status / 100) === 2) {
                return 'Отправка произошла успешно, сетевых ошибок нет';
            }
            throw new Error('Произошла сетевая ошибка, статус: ' + requestResponse.status + '; ' + requestResponse.statusText);
        } catch (err) {
            return err;
        }
    }
}
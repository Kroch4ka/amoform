<?php 
declare(strict_types=1);

use AmoCRM\Filters\ContactsFilter;

error_reporting(E_ALL);

use function Authorize\authorizeAPIClient;


use function AmoCustom\{initExample};
use function DuplicateControl\{getMatchContact, linkNewLeadWithExistingContact};

require_once(realpath(__DIR__ . '/vendor/autoload.php'));
require_once(realpath(__DIR__ . '/helpers/authorizeHelper.php'));
require_once(realpath(__DIR__ . '/helpers/jsonHelper.php'));
require_once(realpath(__DIR__ . '/helpers/contactsHelper.php'));
require_once(realpath(__DIR__ . '/helpers/duplicateControl.php'));

[$name, $email, $phone] = getJSONPostData();

$authorizeClient = authorizeAPIClient();



// Если совпадение есть, то передаём ID первого совпашего контакта дальше для создания сделки, в противном случае всё как обычно
if ($getMatchContact = getMatchContact($authorizeClient, $email, $phone)) {
    linkNewLeadWithExistingContact($authorizeClient, $getMatchContact);
} else {
    $resultFunc = initExample($authorizeClient);
    $resultFunc($name, $phone, $email);
}
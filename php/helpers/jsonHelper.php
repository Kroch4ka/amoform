<?php

/**
 * @return [type]
 * 
 * Получаем JSON с клиента и парсим
 */
function getJSONPostData() {
    $data = json_decode(file_get_contents('php://input'));
    $name = $data->name;
    $email = $data->email;
    $phone = $data->phone;
    return [$name, $email, $phone];
}
<?php

namespace AmoCustom;

/**
 * @param \AmoCRM\Client\AmoCRMApiClient $apiClient
 * @param mixed $name
 * @param mixed $phone
 * @param mixed $email
 * 
 * @return mixed
 * 
 * Создаём контакт AMO с соответствующими параметрами
 */
function createAMOContact(\AmoCRM\Client\AmoCRMApiClient $apiClient, $name, $phone, $email)
{
    $contact = new \AmoCRM\Models\ContactModel();

    $contact->setName($name);

    $customFieldCollection = new \AmoCRM\Collections\CustomFieldsValuesCollection();

    $phoneField = (new \AmoCRM\Models\CustomFieldsValues\MultitextCustomFieldValuesModel())->setFieldCode('PHONE');

    $phoneField->setValues((new \AmoCRM\Models\CustomFieldsValues\ValueCollections\MultitextCustomFieldValueCollection())
        ->add((new \AmoCRM\Models\CustomFieldsValues\ValueModels\MultitextCustomFieldValueModel())
            ->setValue($phone)));

    $emailField = (new \AmoCRM\Models\CustomFieldsValues\MultitextCustomFieldValuesModel())->setFieldCode('EMAIL');


    $emailField->setValues((new \AmoCRM\Models\CustomFieldsValues\ValueCollections\MultitextCustomFieldValueCollection())
        ->add((new \AmoCRM\Models\CustomFieldsValues\ValueModels\MultitextCustomFieldValueModel())
            ->setValue($email)));

    $customFieldCollection->add($emailField)->add($phoneField);

    $contact->setCustomFieldsValues($customFieldCollection);

    try {
        $apiClient->contacts()->addOne($contact);
        return $contact;
    } catch (\AmoCRM\Exceptions\AmoCRMApiException $err) {
        var_dump($err->getDescription());
        die();
    }
}



/**
 * @param \AmoCRM\Client\AmoCRMApiClient $apiClient
 * @param \AmoCRM\Models\LeadModel $contactTargetModel
 * @param string|null $leadName
 * 
 * @return bool
 * 
 * Создаём сделку AMO с соответствующими параметрами
 */
function createAMOLead(\AmoCRM\Client\AmoCRMApiClient $apiClient, \AmoCRM\Models\ContactModel $contactTargetModel)
{
    $lead = new \AmoCRM\Models\LeadModel();
    $lead->setContacts((new \AmoCRM\Collections\ContactsCollection())
        ->add($contactTargetModel));
    try {
        $apiClient->leads()->addOne($lead);
    } catch (\AmoCRM\Exceptions\AmoCRMApiException $err) {
        var_dump($err->getDescription());
        die();
    }
}


/**
 * @param \AmoCRM\Client\AmoCRMApiClient $apiClient
 * 
 * @return callable
 * 
 * Решение задания
 */
function initExample(\AmoCRM\Client\AmoCRMApiClient $apiClient):callable
{
    return function ($name, $phone, $email) use ($apiClient) {
        createAMOLead($apiClient, createAMOContact($apiClient, $name, $phone, $email));
    };
}

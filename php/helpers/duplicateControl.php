<?php

namespace DuplicateControl;

use AmoCRM\Client\AmoCRMApiClient;
use Exception;

use function AmoCustom\createAMOLead;

function getMatchContact(AmoCRMApiClient $apiClient, $email, $phone)
{
    try {
        $contacts = $apiClient->contacts()->get();
    } catch (Exception $err) {
        return null;
    }
    
    $isMatch = false;
    $targetContactsIterable = [];

    foreach ($contacts as $contact) {
        $customFieldValues = $contact->getCustomFieldsValues();
        if ($customFieldValues) {
            foreach ($customFieldValues as $field) {
                if ($field->getFieldCode('PHONE') || $field->getFieldCode('EMAIL')) {
                    $values = $field->getValues()->toArray();
                    array_push($targetContactsIterable, [$contact->getId(), $field->getFieldCode(), array_map(function ($value) {
                        return $value['value'];
                    }, $values)]);
                }
            }
        }
    }
    var_dump($targetContactsIterable);

    foreach ($targetContactsIterable as [$contactID, $contactType, $contactFieldValues]) {
        if ($contactType == 'PHONE') {
            $isMatch = isMatchContact($contactFieldValues, $phone);
            var_dump($phone, $contactFieldValues);
        } else if ($contactType == 'EMAIL') {
            $isMatch = isMatchContact($contactFieldValues, $email);
        }
        if ($isMatch) {
            return $contactID;
        }
    }


    return false;
}


function isMatchContact($contactFieldValues, $contactValue)
{   
    foreach ($contactFieldValues as $value) {
        if ($value == $contactValue) {
            return true;
        }
    }
    return false;
}


function linkNewLeadWithExistingContact(AmoCRMApiClient $apiClient, $contactID) 
{
    $targetContact = $apiClient->contacts()->getOne($contactID);
    
    createAMOLead($apiClient, $targetContact);
}



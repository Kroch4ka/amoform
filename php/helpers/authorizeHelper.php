<?php

namespace Authorize;

require_once realpath(__DIR__ . '/constants.php');
require_once realpath(__DIR__ . '/tokenTools.php');

use AmoCRM\Client\AmoCRMApiClient;
use Exception;

use function TokenTools\{getToken, saveToken};



/**
 * @return [type]
 * 
 * Авторизуемся и получаем доступ к API
 */
function authorizeAPIClient():AmoCRMApiClient
{
        $apiClient = new AmoCRMApiClient(clientID, clientSecret, redirectURI);
        $apiClient->setAccountBaseDomain(subdomain);
        setAuthorizeToken($apiClient, authorizeKey);
        return $apiClient;
}


function setAuthorizeToken($apiClient, $authorizeKey)
{
    if (!getToken()) {
        $accessToken = $apiClient->getOAuthClient()->getAccessTokenByCode($authorizeKey);
        saveToken(
            [
                'accessToken' => $accessToken->getToken(),
                'expires' => $accessToken->getExpires(),
                'refreshToken' => $accessToken->getRefreshToken(),
                'baseDomain' => $apiClient->getAccountBaseDomain(),
            ]
        );
    }
    
    $accessToken = getToken();

    $apiClient->setAccessToken($accessToken)
        ->setAccountBaseDomain($accessToken->getValues()['baseDomain'])
        ->onAccessTokenRefresh(
            function (\League\OAuth2\Client\Token\AccessTokenInterface $accessToken, string $baseDomain) {
                saveToken(
                    [
                        'accessToken' => $accessToken->getToken(),
                        'refreshToken' => $accessToken->getRefreshToken(),
                        'expires' => $accessToken->getExpires(),
                        'baseDomain' => $baseDomain,
                    ]
                );
            }
        );
}
